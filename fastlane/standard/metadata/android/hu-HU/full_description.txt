A WaveUp egy olyan alkalmazás, amely <i>felébreszti a telefont</i> - bekapcsolja a képernyőt -, amikor a közelségérzékelő felett <i>integetsz</i>

Azért fejlesztettem ezt az alkalmazást, mert el akartam kerülni a bekapcsológomb nyomogatását csak azért, hogy egy pillantást vessek az órára - ami nálam elég sokszor előfordul. Vannak más alkalmazások, amelyek pontosan ezt tudják - és még többet is több. Engem a Gravity Screen On / Off inspirált, ami egy <b>nagyszerű</b> alkalmazás. Ugyanakkor óriási rajongója vagyok a nyílt forráskódú szoftvereknek és próbálok mindig ingyenes szoftvert (ami tényleg ingyenes, nem csak úgy, mint az ingyensör) telepíteni a telefonomra, ha lehetséges. Nem találtam nyílt forráskódú alkalmazást, ami pontosan ezt tudta, így hát megcsináltam magam. Ha érdekel, megnézheted a kódot itt:
https://gitlab.com/juanitobananas/wave-up

Csak húzd el a kezed a telefon közelségérzékelője felett oda-vissza, hogy bekapcsold a képernyőt. Ezt hívom <i>integető üzemmódnak</i>, amúgy letiltható a beállításoknál a képernyő véletlen bekapcsolásának elkerülése érdekében.

A képernyő úgy is bekapcsolható, ha a telefonod előveszed a zsebedből vagy egy táskából. Ezt hívom <i>zseb üzemmódnak</i> és a beállításoknál szintén letiltható.

 Alapértelmezés szerint mindkét üzemmód engedélyezett.

Az alkalmazás lezárja a telefont és kikapcsolja a képernyőt, ha egy másodpercre (vagy egy meghatározott időre) eltakarod a közelségérzékelőt. Ennek nincs külön neve, de ennek ellenére ez is megváltoztatható a beállításoknál. Alapértelmezés szerint nem bekapcsolt.

Akik még soha nem hallottak a közelségérzékelőről: ez egy kicsi szenzor a telefonon nagyjából ott, ahol a füled van, amikor telefonon beszélsz. Alig látható és az a feladata, hogy segítségével a telefonod hívás közben zárolja a képernyőt, elkerülendő az arcoddal vagy a füleddel történő véletlen érintések érzékelését .

<b>Eltávolítás</b>

Az alkalmazás eszközadminisztrátori engedélyt használ. Ezért nem távolítható el a "hagyományos” módon.

Az eltávolításhoz nyisd meg, majd használd a menü alján található „WaveUp eltávolítása” gombot.

<b>Ismert problémák</b>

Sajnos néhány okostelefon a közelségérzékelő folyamatos figyelésekor bekapcsolva tartja a CPU-t. Ezt <i>wake lock</i>-nak (ébrentartás) hívják és az akkumulátor gyors merülését okozza. Ez nem az én hibám és semmit sem tehetek ellene. Más telefonok "alszanak", amikor a képernyő ki van kapcsolva, miközben továbbra is figyelik a közelségérzékelőt. Ebben az esetben az akkumulátor merülése gyakorlatilag nulla.

<b>Szükséges Android engedélyek</b>

▸ WAKE_LOCK a képernyő bekapcsolásához
▸ USES_POLICY_FORCE_LOCK az eszköz zárolásához
▸ RECEIVE_BOOT_COMPLETED az automatikus indításhoz (ha bejelölt)
▸ READ_PHONE_STATE a WaveUp szüneteltetéséhez hívás közben

<b>Egyéb megjegyzések</b>

Ez az első, általam készített Android app, szóval légy óvatos!

Továbbá ez az első próbálkozásom a nyílt forráskód világában. Végre!

Szívesen venném, ha bármilyen visszajelzést adnál, vagy bármilyen módon hozzájárulnál a munkámhoz!

Kösz, hogy elolvastad!

A nyílt forráskód menő!!!

<b>Fordítások</b>

Klassz lenne, ha segítenél a WaveUp lefordításában az anyanyelvedre (vagy akár az angol verzió felülvizsgálatában).
A fordításhoz két projekt érhető el a Transifex-en: https://www.transifex.com/juanitobananas/waveup/ és https://www.transifex.com/juanitobananas/libcommon/.

<b>Köszönetnyilvánítások</b>

Külön köszönet nekik:

Lásd: https://gitlab.com/juanitobananas/wave-up/#acknowledgments