New in 3.2.11
★ Upgrade a bunch of dependencies.

New in 3.2.10
★ Update some translations.
★ Upgrade some dependencies.

New in 3.2.9
★ Update some translations.
★ Fix small Android 11 compatibility bugs.

New in 3.2.8
★ Update some translations.
★ Small bug fix (MainActivity leak).
