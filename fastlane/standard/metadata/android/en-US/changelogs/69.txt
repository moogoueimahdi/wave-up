Dear Pie users, if you had hidden the notification, you might have to hide it again. Due to a big Android change (not allowing apps to read sensors in the background) has forced WaveUp to always show a notification. Hopefully, it'll still work if you hide it with the new setting.

New in 2.6.1
★ Make Pie compatible. Add an option to go to Android's notification settings  and remove "hide notification" option (only for Pie devices or newer).