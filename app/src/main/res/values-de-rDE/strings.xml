<resources>
    <string name="prefs_service">Service</string>
    <string name="pref_enable">Einschalten</string>

    <string name="prefs_modes">WaveUp-Modi</string>
    <string name="pref_wave_mode">Winkmodus</string>
    <string name="pref_wave_mode_summary">Display einschalten, wenn man ein mal mit der Hand über den Proximity-Sensor winkt.    </string>
    <string name="pref_pocket_mode">Taschenmodus</string>
    <string name="pref_pocket_mode_summary">Display einschalten, wenn man das Gerät aus der Tasche holt.    </string>

    <string name="pref_lock_screen">Gerät sperren</string>
    <string name="pref_lock_screen_mode_summary">Display ausschalten und Gerät sperren, wenn der Proximity-Sensor bedeckt wird.</string>
    <string name="pref_lock_screen_when_landscape">Sperren in Landscape-Modus</string>
    <string name="pref_lock_screen_when_landscape_summary">Wenn deaktiviert, wird vermieden, dass das Gerät sich sperrt, während es horizontal gehalten wird, z.B. um Media zu schauen.</string>
    <string name="pref_lock_screen_with_power_button_as_root">Workaround für Fingerprint- und Smart-Lock</string>
    <string name="pref_lock_screen_with_power_button_as_root_summary">Simuliert den An/Aus-Knopf, um den Bildschirm auszuschalten. Benötigt root.</string>
    <string name="pref_lock_screen_vibrate_on_lock">Vorm Sperren vibrieren</string>
    <string name="pref_lock_screen_vibrate_on_lock_summary">Das Gerät vibriert, wenn der Proximity-Sensor bedeckt wird, um versehentliche Sperrungen zu vermeiden.</string>
    <string name="pref_lock_screen_app_exception">Ausgeschlossene Apps</string>
    <string name="pref_lock_screen_app_exception_summary">Sperren vermeiden, wenn einer der ausgewählten Apps im Vordergrund läuft. Tippen Sie hier, um die Liste einzustellen.</string>
    <string name="pref_sensor_cover_time_before_locking_screen">Deckzeit zum Sperren des Geräts</string>
    <string name="pref_sensor_cover_time_before_locking_screen_summary">Zeit der Proximity-Sensor bedeckt werden muss, damit WaveUp das Gerät sperrt. Aktueller Wert: %s.</string>
    <string name="pref_notification_section">Benachrichtigung</string>
    <string name="pref_show_notification">Benachrichtigung zeigen</string>
    <string name="pref_show_notification_summary">Benachrichtigung immer zeigen, um WaveUp kontrollieren zu können und um sicherzustellen, dass WaveUp läuft.</string>
    <!-- From Pie onwards, we just show the Android system settings and always use a ForegroundService -->
    <string name="pref_show_notification_v28">Benachrichtigung-Einstellungen</string>
    <string name="pref_show_notification_summary_v28">Tippen Sie hier, um WaveUps System-Benachrichtigung-Einstellungen zu öffnen.\n\n<small>Hinweis vom Entwickler: ab Android 9, werden Hintergrund-Apps nicht erlaubt, auf die Sensoren zu hören ohne eine Benachrichtigung zu zeigen. Allerdings können die NutzerInnen diese mit den System-Einstellungen ausblenden. Möglicherweise funktioniert dies auf Ihrem Gerät nicht. Bitte schalten Sie in diesem Fall die Benachrichtigungen erneut an.</small></string>

    <string name="wave_up_service_started">WaveUp wird ausgeführt</string>
    <string name="wave_up_service_stopped">WaveUp wird nicht ausgeführt</string>
    <string name="tap_to_open">Tippen um WaveUp zu öffnen</string>
    <string name="pause">Pause</string>
    <string name="resume">Weiter</string>
    <string name="disable">Stop</string>

    <string name="lock_admin_rights_explanation"><b>Sperre Gerät: Geräteadministrator</b>\n\nUm den Bildschirm sperren zu können, muss WaveUp als Geräteadministrator eingestellt sein.\n\nSobald eine App Geräteadministrator ist, kann man diese nicht normal entfernen, ohne die Rechte dafür wieder zu entfernen.\n\nWenn du diese Rechte wieder entfernen möchtest, geh zu <i>Einstellungen → Sicherheit → Geräteadministrators</i> und entferne WaveUp.\n\nWenn du WaveUp entfernen möchtest, klicke unten auf den \'WaveUp entfernen\' Button, welcher die Geräteadministratorrechte entfernt <i>und</i> WaveUp direkt entfernt.\n\nBist du sicher das du WaveUp als Geräteadministrator hinzufügen möchtest?</string>
    <string name="something_went_wrong">Etwas lief schief</string>
    <string name="root_access_failed">Root-Zugriff wurde nicht erlaubt</string>

    <string name="uninstall_button">WaveUp deinstallieren</string>
    <string name="removed_device_admin_rights">WaveUp ist kein Gerätadministrator mehr. Drücken Sie die "Gerät sperren" Option, um es wieder einzurichten</string>

    <!-- Phone permission dialog -->
    <string name="phone_permission_yes">Anfrage</string>
    <string name="phone_permission_no">Ignorieren</string>
    <!-- I put this array here instead of in arrays.xml because it has to be translated. -->
    <string-array name="sensor_cover_time_entries">
        <item>Sofort</item>
        <item>0,5 Sekunden</item>
        <item>1 Sekunde</item>
        <item>1,5 Sekunden</item>
        <item>2 Sekunden</item>
        <item>5 Sekunden</item>
    </string-array>

    <!-- Vibrate on lock -->
    <string name="pref_vibrate_on_lock_time_title">Vorm Sperren vibrieren</string>
    <string name="pref_number_of_waves">Winkzahl</string>
    <string name="pref_number_of_waves_summary">Anzahl der Winken (bedecken und aufdecken), um das Display einzuschalten. Aktueller Wert: %s.</string>

    <string-array name="number_of_waves_entries">
        <item>1 (bedecken, aufdecken)</item>
        <item>2 (bedecken, aufdecken, bedecken, aufdecken)</item>
        <item>3 (bedecken, aufdecken, bedecken, aufdecken, bedecken, aufdecken)</item>
    </string-array>

    <string name="privacy_policy_menu_item">Datenschutzerklärung</string>
    <string name="licenses_menu_item">Lizenzen</string>
    <!-- Excluded apps from locking -->
    <string name="untitled_app">Untitled App</string>
    <string name="exclude_apps_activity_title">Apps ausschließen</string>
    <string name="excluded_apps">Ausgeschlossene Apps</string>
    <string name="not_excluded_apps">Nicht ausgeschlossene Apps</string>
    <string name="exclude_apps_text">Es sind keine ausgeschlossene Apps im Moment.
\n\nWährend eine der hier aufgelisteten Apps im Vordergrund läuft, wird WaveUp das Bildschirm Ihres Gerätes nicht sperren.
\n\nTippen Sie einfach auf eine App unter der Liste der nicht ausgeschlossenen Apps, um diese auszuschließen. Tippen Sie noch mal, wenn sie die App nicht mehr ausschließen möchten. </string>
    <string name="usage_access_explanation">
        <b>Ausnahme von Apps durch Sperrung: Nutzungszugriff</b>
        \n\nUm zu verhindern, dass der Bildschirm <i>nicht</i> während einer spezifischen App gesperrt wird, benögtigt WaveUp <i>Nutzungszugriff</i>.
        \n\nWillst du WaveUp 3Nutzungszugriff3 gewähren? Du musst diese Rechte im nächsten Fenster selbstständig gewähren.
    </string>


    <!-- App shortcut -->
    <string name="lock_now">Jetzt sperren</string>

    <!-- Lack of proximity sensor -->
    <string name="missing_proximity_sensor_title">Kein Proximity-Sensor gefunden</string>
    <string name="missing_proximity_sensor_text">WaveUp kann leider ohne Proximity-Sensor nicht arbeiten. Entweder Ihr Gerät hat Keinen oder WaveUp kann ihn nicht betreiben. Wir bitten um Verzeihung.</string>
</resources>
